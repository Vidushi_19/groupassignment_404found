package UserInterface;

import Business.Users.Admin;
import Business.Users.Customer;
import Business.Users.Supplier;
import java.awt.CardLayout;
import java.awt.Color;
import Business.Abstract.User;
import java.awt.Component;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import javax.swing.JPanel;


public class AdminCreateScreen extends javax.swing.JPanel {

    /**
     * Creates new form AdminScreen
     */
    private JPanel panelRight;
    private Admin admin;
    public AdminCreateScreen(JPanel panelRight, Admin admin) {
        initComponents();
        this.panelRight = panelRight;
        this.admin = admin;
        checkForButtonVisibility();
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        radioUserGrp = new javax.swing.ButtonGroup();
        btnCreate = new javax.swing.JButton();
        txtUser = new javax.swing.JTextField();
        txtPword = new javax.swing.JTextField();
        txtRePword = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        radioCustomer = new javax.swing.JRadioButton();
        radioSupplier = new javax.swing.JRadioButton();
        btnBack = new javax.swing.JButton();

        btnCreate.setText("Create");
        btnCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateActionPerformed(evt);
            }
        });

        txtRePword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtRePwordKeyTyped(evt);
            }
        });

        jLabel1.setText("username:");

        jLabel2.setText("password:");

        jLabel3.setText("re-enter password :");

        radioUserGrp.add(radioCustomer);
        radioCustomer.setText("Customer");
        radioCustomer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radioCustomerActionPerformed(evt);
            }
        });

        radioUserGrp.add(radioSupplier);
        radioSupplier.setText("Supplier");

        btnBack.setText("< BACK");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPword, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtRePword, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnCreate, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(radioSupplier)
                            .addComponent(radioCustomer)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(17, 17, 17)
                        .addComponent(btnBack)))
                .addGap(76, 76, 76))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtUser, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtPword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtRePword, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioCustomer)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(radioSupplier)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCreate)
                .addGap(47, 47, 47))
        );
    }// </editor-fold>//GEN-END:initComponents
    
    private boolean passwordPatternCorrect(){
        Pattern p = Pattern.compile("[A-Za-z0-9+_$]{1,100}");
        Matcher m = p.matcher(txtPword.getText());
        if (!m.matches()) {
            System.out.println("There is a special character in entered password");
            return false;}
        else{
            System.out.println("There is no special char.");
            return true;
        }
    }
    private boolean userExists(){
      for(User u : admin.getCustDir().getCustomerList()){
          if(u.getUserName().equalsIgnoreCase(txtUser.getText()))
              return true;
      }
      for(User u : admin.getSuppDir().getSupplierList()){
          if(u.getUserName().equalsIgnoreCase(txtUser.getText()))
              return true;
      }
      return false;
    }
    
    private boolean usernamePatternCorrect(){
        Pattern p = Pattern.compile("^([a-zA-Z0-9_\\-\\.]+)@([a-zA-Z0-9_\\-\\.]+)\\.([a-zA-Z]{2,5})$");
        Matcher m = p.matcher(txtUser.getText());
        return m.find();
        
    }
    private void btnCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateActionPerformed
        // TODO add your handling code here:
        if(passwordPatternCorrect()==false){
            JOptionPane.showMessageDialog(this,"password must contain alphanumeric values");
            return;
        }else if(!txtPword.getText().equals(txtRePword.getText())){
            JOptionPane.showMessageDialog(this,"passwords don't match !!");
            return;
        }
        else if(!usernamePatternCorrect()){
            JOptionPane.showMessageDialog(this, "Invalid username");
            return;
                }
        else{
        try {
        if(radioUserGrp.isSelected(radioCustomer.getModel())){           
            Customer c = new Customer(new Date(),txtPword.getText(),txtUser.getText());
            if(userExists()){
            JOptionPane.showMessageDialog(this, "User exists in the record, try another");}
            else{
            admin.getCustDir().getCustomerList().add(c);
            JOptionPane.showMessageDialog(this,"customer created successfully");}}
        else if(radioUserGrp.isSelected(radioSupplier.getModel())){
            Supplier s = new Supplier(txtPword.getText(),txtUser.getText());
            if(userExists()){
            JOptionPane.showMessageDialog(this, "User exists in the record, try another");}
            else{
            admin.getSuppDir().getSupplierList().add(s);
            JOptionPane.showMessageDialog(this,"Supplier created successfully");}
        }
        else JOptionPane.showMessageDialog(this, "Please select a role of the user");
        }catch (Exception ex) {
                System.out.println(ex.getMessage());
            }}
        resetAll();
        
    }//GEN-LAST:event_btnCreateActionPerformed

    private void radioCustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radioCustomerActionPerformed
        
    }//GEN-LAST:event_radioCustomerActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        this.panelRight.remove(this);
        CardLayout layout = (CardLayout)this.panelRight.getLayout();
//        get all components of cardlayout
        Component[] comps = this.panelRight.getComponents();
//        traverse all components to find manageProdPanel;
        for(Component cmp:comps){
            if(cmp instanceof AdminMainScreen){
            AdminMainScreen mPanel=(AdminMainScreen)cmp;
            mPanel.populateCustomer();
            mPanel.populateSupplier();
            
        }
        }
        layout.previous(this.panelRight);
    }//GEN-LAST:event_btnBackActionPerformed

    private void txtRePwordKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtRePwordKeyTyped
        // TODO add your handling code here:
        checkForButtonVisibility();
        String check = ""+evt.getKeyChar();
        String comp = txtRePword.getText();
        if(!check.isEmpty())
            comp+=check;
        if(txtPword.getText().equalsIgnoreCase(comp)){
            txtRePword.setBackground(Color.white);
            btnCreate.setEnabled(true);
        }else{
            txtRePword.setBackground(Color.red);
            btnCreate.setEnabled(false);
        }
             checkForButtonVisibility();
             
    }//GEN-LAST:event_txtRePwordKeyTyped
    private void checkForButtonVisibility(){
        if(txtUser.getText().isEmpty() && txtPword.getText().isEmpty() && txtPword.getText().isEmpty())
            btnCreate.setEnabled(false);
        else
            btnCreate.setEnabled(true);
    }  
    
    private void resetAll(){
        txtPword.setText("");
        txtRePword.setText("");
        txtUser.setText("");
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCreate;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JRadioButton radioCustomer;
    private javax.swing.JRadioButton radioSupplier;
    private javax.swing.ButtonGroup radioUserGrp;
    private javax.swing.JTextField txtPword;
    private javax.swing.JTextField txtRePword;
    private javax.swing.JTextField txtUser;
    // End of variables declaration//GEN-END:variables
}
